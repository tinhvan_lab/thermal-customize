const VIP_type = "VIP";
const title = "HiFACE Thermal";
const hiface_lib = [8];

// const hiface_host = "//192.168.99.9"

class AlbumList {
    get hiface_img() {
        return this._hiface_img;
    }

    set hiface_img(value) {
        this._hiface_img = value;
    }

    get photoList() {
        return this._photoList;
    }

    set photoList(value) {
        this._photoList = value;
    }

    constructor() {

        console.log("begin")

    }

    _hiface_img = "";
    _photoList = [];

    calculatePagesCount(pageSize, totalCount) {
        // we suppose that if we have 0 items we want 1 empty page
        let total = totalCount < pageSize ? 1 : Math.ceil(totalCount / pageSize);
        // this._list = total;
        return total;
    }

    get_albumList() {

    }


    get_photoList() {

        let url = hiface_host + "/info/albumList";

        console.log(url);

        let param = {
            "type": "face",
            "success": true,
            "libs": hiface_lib,
            "pageNum": 1,
            "pageSize": 500,
            "photoName": ""
        };
        let photolist = [];

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify(param),
            processData: false,
            success: function (data, textStatus, jQxhr) {
                console.log(data);
                // photolist = data;
                HiFACE_Global.photoList = data;

                /*FULL*/
                HiFACE.displayFull();

            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });

        // this._photoList = photolist;
    }


    displayFull() {

        if (HiFACE_Global.photoList.length == 0) return;

        let identify = $(".call-list-cont li:first").find("img:last").attr("src");
        let person = $.grep(HiFACE_Global.photoList.records,
            function (e) {
                return e.photoUrl == identify;
            });

        console.log(person);

        /*FINDING RESULT*/
        if (person.length != 0) {

            let person_info = person[0];
            let name = person_info.name;
            let style = {

                "text-align": "center",
                "padding": "5px",
                "font-size": "16pt",
            }

            /*CHECK IF INSERT AlREADY*/
            if ($(".call-list-cont li:first").find(".fullname").length == 0) {

                /*REPLACE INFO*/
                $(".call-list-cont li:first").find(".el-tooltip:first").html("");

                /*INSERT NAME*/
                let info = $("<h1/>").addClass("fullname").text(name).css(style);
                $(".call-list-cont li:first").find(".call-pic").after(info)
            }
        }

        return;
    }

    popUp() {

        /*CLICK TO USER CARD*/
        $(".call-list-cont li:first").trigger("click");

        /*render new UI*/
        setTimeout(this.vip_render, 200);

        /*CLOSE WINDOW*/
        if (popup_flag == false) {

            popup_flag = true;

            setTimeout(function () {

                $(".el-dialog__header button").trigger("click")
                popup_flag = false;

            }, 5000);
        }
        return;

    }

    checkVIP() {

        let type = $(".call-list-cont li:first").find(".el-tooltip:last").html();

        if (type.indexOf(VIP_type) != -1) {

            this.popUp();
        }
    }

    // RENDER VIP POP-UP
    vip_render() {

        // let name = $(".call-list-cont li:first").find(".el-tooltip:first").html();

        let name = $(".info-box:last .tray-text:first").text();
        let css = {
            "color": "#f5b342",
            "font-size": "30pt"

        }
        $(".el-dialog__title:first").text(`Chào mừng đồng chí ${name}`).css(css);
        $(".el-dialog__header:first").css("text-align", "center");

        return;

    }

    // VIETNAMESE
    translator() {

        $("#app > section > div > div.realtime-alarm-count > div > div > div.capture-count > b").html("Khách")
        $("#app > section > div > div.realtime-alarm-count > div > div > div.warn-count > b").html("Cảnh báo")

        $("#app > section > div > div.realtime-alarm-main > div.realtime-alarm-content > div.realtime-alarm-targets > div.title > div > b").html("Người đi qua")

        $(".call-list-cont li").find("span:contains(No mask)").each(function () {

            $(this).text("Không đeo khẩu trang").css("display", "inline-block");
        })

        return;
    }

}

let HiFACE_Global = {
    photoList: []
};
let popup_flag = false;

$(function () {

    // DISABLE MENU
    let hiface_ui = function () {

        $("#app > section > header > div > div.menu-wrapper > ul").hide();
        $("#app > section > header > div > div.system-name").hide();

        // $(".logo img").hide();
        document.title = title;
    }


    hiface_ui()

    /*MAIN*/
    HiFACE.get_photoList();

    /*DISPALY FEED NEW PHOTO*/
    let newImage_event = function () {

        let current = $(".call-list-cont li:first").find("img");
        let img = current.attr("src");

        /*DISPLAY FULL INFORMATION OF PERSON*/

        if (HiFACE.hiface_img != img) {

            // console.log("new image: " + img);
            HiFACE.hiface_img = img

            /*DISPLAY*/
            HiFACE.displayFull();

            /*CHECK VIP*/
            HiFACE.checkVIP();

        }

        return;
    }


    setInterval(function () {
        HiFACE.translator()
        newImage_event();

    }, 500);
});


//MAIN FUNC
let HiFACE = new AlbumList();
// let hiface_logo = "data:image/jpeg;base64,"



