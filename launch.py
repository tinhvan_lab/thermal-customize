import time
import sys
import requests
from selenium import webdriver

FULL_SCREEN = False

# Login to get SessionID
url = 'http://192.168.99.9'
response = requests.post(url + '/login', json={'username': 'admin', 'password': '71ce2ad3d895a0e9bbbe7d1dc360472a',
                                               'authentication': 'basic'})
try:
    SessionID = response.cookies['SESSIONID']
except:
    print('ERROR: wrong username/password!')
    sys.exit()

# Set options and launch Chrome
chrome_options = webdriver.ChromeOptions()
chrome_options.add_experimental_option("excludeSwitches", ['enable-automation', 'enable-logging'])
# chrome_options.add_argument("--kiosk")
driver = webdriver.Chrome(options=chrome_options)

# Open URL with Session ID
driver.get(url + '/dummy_page')
driver.add_cookie({'name': 'SESSIONID', 'value': SessionID})
driver.get(url)

# Find and modify elements
driver.implicitly_wait(10)  # Wait for page loading
elements = driver.find_elements_by_class_name('system-name')

# driver.execute_script("arguments[0].style.visibility = 'hidden';", elements[0])

# if FULL_SCREEN:
#     elements = driver.find_elements_by_class_name('el-dropdown-link')
#     elements[0].click()
#     driver.implicitly_wait(10)  # Wait for dropdown menu to be shown
#     elements = driver.find_elements_by_class_name('el-dropdown-menu__item')
#     elements[0].click()
# else:
#     elements = driver.find_elements_by_class_name('user-panel')
#     driver.execute_script("arguments[0].style.visibility = 'hidden';", elements[0])
# DEFINE SCRIPT
# driver.execute_script()


jquery = open("assets/jquery.js", "r").read()
driver.execute_script(jquery)

logo = open("assets/image.js", "r", encoding='utf-8').read()
# print(logo)
driver.execute_script(logo)

# time.sleep(2)

hiface = open("assets/hiface_thermal.js", "r", encoding='utf-8').read()
# time.sleep(1)

driver.execute_script(
    'const hiface_host = "' + url + '";' +
    hiface
)

# Disable F5 (reload page) and Ctrl-U (view code)
# driver.execute_script('document.onkeydown=function(e){ if (((e.which || e.keyCode) == 116) || (e.ctrlKey && e.keyCode == 85)) e.preventDefault(); }')

# Wait until browser closed
while True:
    logs = driver.get_log('driver')
    if logs and logs[0]['message'].find('disconnected:') != -1:
        break
    time.sleep(1)
driver.quit()
